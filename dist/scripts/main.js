'use strict';

// ready
$(document).ready(function () {

    // anchor
    // $(".anchor").on("click","a", function (event) {
    //     event.preventDefault();
    //     var id  = $(this).attr('href'),
    //         top = $(id).offset().top;
    //     $('body,html').animate({scrollTop: top + 40}, 1000);
    // });
    // anchor

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    $('.carousel').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3
    });
    // slider

    // range slider
    if ($("#rangeSlider").length) {
        var range = document.getElementById('rangeSlider');
        var start = document.getElementById('start');
        noUiSlider.create(range, {
            start: [680000],
            connect: [true, false],
            step: 10000,
            range: {
                'min': 0,
                'max': 1335000
            }
        });
        range.noUiSlider.on('update', function (values, handle) {
            var value = values[handle];
            start.value = Math.round(value) + ' ₽';
        });
        start.addEventListener('change', function () {
            range.noUiSlider.set([null, this.value + ' ₽']);
        });
    }
    // range slider
});
// ready
//# sourceMappingURL=main.js.map